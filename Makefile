main: main.o lib.o dict.o
	ld -o main main.o lib.o dict.o
	
main.o: main.asm lib.o dict.o words.inc colon.inc lib.inc
	nasm -felf64 -o main.o main.asm

dict.o: dict.asm lib.o
	nasm -felf64 -o dict.o dict.asm
	
lib.o: lib.asm 
	nasm -felf64 -o lib.o lib.asm
