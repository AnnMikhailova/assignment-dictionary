%include "lib.inc"
global find_word

section .text

find_word:              ;rdi - str ptr, rsi - dictionary ptr
    xor rax, rax
.loop:
    test rsi, rsi       ;check if end of dictionary
    je .end
    
    push rdi
    push rsi
    add rsi, 8          ;get current str ptr
    call string_equals  ;check if found
    pop rsi
    pop rdi

    cmp rax, 1  
    je .found 

    mov rsi, [rsi]    ;get next elem str by ptr on next elem
    jmp .loop

.found: 
    mov rax, rsi        ;result is ptr on elem
.end:
    ret

