%include "words.inc"
%include "lib.inc"
extern find_word
global _start

%define buff_size 256

section .data
    buff: times buff_size db 0

section .rodata
    ex_cannot_read_word_text: db "Can't read your word", 0
    ex_dict_no_such_key_text: db "Dictionary doesn't contain this key", 0

section .text

_start:
    mov rdi, buff               ;fill buff from stdin
    mov rsi, buff_size
    call read_word
    test rax, rax
    je .ex_cannot_read_word     ;catch exception

    mov rdi, buff               ;find string from buff in dict
    mov rsi, dict
    call find_word
    test rax, rax
    je .ex_dict_no_such_key     ;catch exception

    add rax, 8

    mov rdi, rax
    push rax
    call string_length
    mov rdi, rax
    pop rax
    add rax, rdi
    inc rax

    mov rdi, rax                ;print value
    mov rsi, 1
    call print_string
    call print_newline
    jmp .end


.ex_dict_no_such_key:
    mov rdi, ex_dict_no_such_key_text
    jmp .print_exception

.ex_cannot_read_word:
    mov rdi, ex_cannot_read_word_text

.print_exception:
    mov rsi, 2
    call print_err
    call print_newline


.end:
   call exit

