%define prev 0      ;label on previous elem

%macro colon 2      

%ifstr %1           ;check if key
    %ifid %2        ;check if label

        %2: dq prev ;set label on previous elem to new elem
        db %1, 0    ;set key value

        %define prev %2

%endmacro